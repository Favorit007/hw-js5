//     ДР №5
//    Теория

// 1.Метод объекта - это функция внутри объекта. 

// 2. В объекте может храниться любой тип данных из существующих.

// 3. Объект хранит не данные, а ссылки на данные и копирование происходит по ссылке.


//                                     Практика
//                                        #1

function createNewUser() {
    const firstName = prompt("What is your name?");
    const lastName = prompt("What is your last name?");

    const newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
    }
    return newUser;
};

const user = createNewUser()
// console.log(user);
console.log(user.getLogin())